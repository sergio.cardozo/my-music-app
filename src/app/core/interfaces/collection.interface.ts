export interface collection {
  collaborative: boolean;
  description: string;
  external_urls: object;
  href: string;
  id: string;
  images: any[];
  name: string;
  owner: object;
  tracks: object;
  type: string;
  uri: string;
  album_type?: string;
  artists?: any[];
  available_markets?: any[];
  release_date?: string;
  release_date_precision?: string;
  total_tracks?: number;
}

export interface favoritesCollection {
  description: string;
  images: any[];
  name: string;
  type: string;
}
