import { Albums } from './albums.interface';
import { Playlists } from './playlists.interface';
export interface browseResponse {
  categories?: Categories;
  albums?: Albums;
  playlists: Playlists;
}

export interface Categories {
  href: string;
  items: categoriesItem[];
  limit: number;
  next: string;
  offset: number;
  previous: null;
  total: number;
}

export interface categoriesItem {
  href?: string;
  icons?: Icon[];
  id: string;
  name: string;
}

export interface Icon {
  height: number | null;
  url: string;
  width: number | null;
}
