export interface images {
  url: string;
  height: number;
  width: number;
}
