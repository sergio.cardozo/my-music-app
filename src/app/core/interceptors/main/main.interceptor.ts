import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpContextToken,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, Observable, switchMap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageService } from '@shared/services/storage/storage.service';
import { AuthService } from '@shared/services/auth/auth.service';

export const AUTHENTICATION_HEADER = new HttpContextToken(() => false);

@Injectable()
export class MainInterceptor implements HttpInterceptor {
  constructor(private storage: StorageService, private auth: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (request.context.get(AUTHENTICATION_HEADER) === true) {
      return next.handle(this.headerAuth(request));
    } else {
      return next.handle(this.headerToken(request)).pipe(
        catchError((err: HttpErrorResponse) => {
          if (
            (err.status === 401 || err.status === 403) &&
            this.storage.getRefreshToken()
          ) {
            return this.handleRefreshToken(request, next);
          }
          return throwError(() => err);
        })
      );
    }
  }

  private headerAuth(request) {
    const requestClone = request.clone({
      setHeaders: {
        Authorization: `Basic ${Buffer.from(
          `${environment.clientId}:${environment.clientSecret}`
        ).toString('base64')}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
    return requestClone;
  }

  private headerToken(request, token: string = this.storage.getToken()) {
    const requestClone = request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
    return requestClone;
  }

  private handleRefreshToken(request: HttpRequest<unknown>, next: HttpHandler) {
    return this.auth.refreshToken().pipe(
      switchMap((data: string) => {
        this.storage.saveToken(data);
        return next.handle(this.headerToken(request, data));
      }),
      catchError((err) => {
        this.auth.logOut();
        return throwError(() => err);
      })
    );
  }
}
