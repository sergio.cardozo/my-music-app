import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '@shared/services/storage/storage.service';

@Injectable({
  providedIn: 'root',
})
export class HomeGuard implements CanActivate {
  constructor(private router: Router, private storageService: StorageService) {}

  reDirect() {
    this.router.navigate(['/', 'login']);
  }

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (
      !this.storageService.getToken() ||
      !this.storageService.getRefreshToken()
    ) {
      this.reDirect();
    }
    return true;
  }
}
