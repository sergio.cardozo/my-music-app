import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from '@modules/home/pages/home-page/home-page.component';
import { HomeGuard } from '@core/guards/home/home.guard';
import { LoginGuard } from '@core/guards/login/login.guard';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    loadChildren: () =>
      import('@modules/home/home.module').then((m) => m.HomeModule),
    canActivate: [HomeGuard],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('@modules/login/login.module').then((m) => m.LoginModule),
    canActivate: [LoginGuard],
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
