import { Component, OnInit } from '@angular/core';
import { FrameService } from '@shared/services/frame/frame.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  media = this.frame.selectedMediaPlay;
  id: string = '';
  type: string = '';
  urlFrame = `https://open.spotify.com/embed/`;
  constructor(private frame: FrameService) {}

  ngOnInit(): void {
    this.frame.selectedMediaPlay.subscribe((data) => {
      this.id = data['id'];
      this.type = data['type'];
    });
  }
}
