import { Component, OnInit } from '@angular/core';
import { favoritesCollection } from '@core/interfaces/collection.interface';

@Component({
  selector: 'app-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.scss'],
})
export class FavoritesPageComponent implements OnInit {
  collection: favoritesCollection = {
    type: 'playlist',
    description: '',
    name: 'Liked Songs',
    images: [
      { url: 'https://t.scdn.co/images/3099b3803ad9496896c43f22fe9be8c4.png' },
    ],
  };

  constructor() {}

  ngOnInit(): void {}
}
