import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { BrowseService } from '@shared/services/browse/browse.service';
import { Categories, categoriesItem } from '@core/interfaces/browse.interface';

@Component({
  selector: 'app-browse-page',
  templateUrl: './browse-page.component.html',
  styleUrls: ['./browse-page.component.scss'],
})
export class BrowsePageComponent implements OnInit {
  categories: categoriesItem[] = [
    {
      name: 'Ultimos Lanzamientos',
      id: 'new-releases',
    },
  ];
  nextCategories = '';
  offset = 0;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private browse: BrowseService
  ) {}

  ngOnInit(): void {
    this.getCategories();
    this.document.documentElement.style.setProperty('--color-1', '#1ed760');
    this.document.documentElement.style.setProperty('--color-7', '#202020');
  }

  onScrollDown(): void {
    if (this.nextCategories) {
      this.offset += 3;
      this.getCategories();
    }
  }

  getCategories(): void {
    this.browse.getCategories(this.offset).subscribe((data: Categories) => {
      this.categories.push(...data.items);
      this.nextCategories = data.next;
    });
  }
}
