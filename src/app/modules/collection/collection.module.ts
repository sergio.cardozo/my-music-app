import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollectionRoutingModule } from './collection-routing.module';
import { CollectionPageComponent } from './pages/collection-page/collection-page.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [CollectionPageComponent],
  imports: [CommonModule, CollectionRoutingModule, SharedModule],
})
export class CollectionModule {}
