import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { AlbumsService } from '@shared/services/albums/albums.service';
import { PlaylistsService } from '@shared/services/playlists/playlists.service';
import { albumItem } from '@core/interfaces/albums.interface';
import { playlistItem } from '@core/interfaces/playlists.interface';
@Component({
  selector: 'app-collection-page',
  templateUrl: './collection-page.component.html',
  styleUrls: ['./collection-page.component.scss'],
})
export class CollectionPageComponent implements OnInit {
  id: number;
  type: string;
  collection: albumItem | playlistItem;
  colorCover = { r: 255, g: 255, b: 255 };
  collections = {
    album: () => {
      this.albums.getAlbum(this.id).subscribe((data: albumItem) => {
        this.collection = data;
      });
    },
    playlist: () => {
      this.playlists.getPlaylist(this.id).subscribe((data: playlistItem) => {
        this.collection = data;
      });
    },
  };
  constructor(
    private route: ActivatedRoute,
    private albums: AlbumsService,
    private playlists: PlaylistsService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.type = params['type'];
      this.collections[this.type]();
    });
  }
}
