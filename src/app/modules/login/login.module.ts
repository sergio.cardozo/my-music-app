import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CallbackComponent } from './pages/callback/callback.component';
@NgModule({
  declarations: [LoginPageComponent, CallbackComponent],
  imports: [CommonModule, LoginRoutingModule],
})
export class LoginModule {}
