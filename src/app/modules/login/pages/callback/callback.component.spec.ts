import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@shared/services/auth/auth.service';
import { of, EMPTY, throwError } from 'rxjs';
import { CallbackComponent } from './callback.component';
import { Inject } from '@angular/core';
import { tokenResponse } from '../../../../core/interfaces/auth.interface';

//   it('Debe llamar al servicio para obtener el codigo', () => {
//     const spy = spyOn(authSvc, 'getTokens').and.callFake(() => {
//       return EMPTY;
//     });
//     component.getTokens();

//     expect(spy).toHaveBeenCalled();
//   });
// });

describe('CallbackComponent', () => {
  let component: CallbackComponent;
  let fixture: ComponentFixture<CallbackComponent>;
  let authSvc: AuthService;
  let activatedRouteSpy = {
    queryParams: of({ code: '' }),
  };

  beforeEach(() => {
    // Con el TestBed le decimos a angular que compile la forma total del componente
    TestBed.configureTestingModule({
      declarations: [CallbackComponent],
      imports: [HttpClientModule],
      // Aqui van los servicios
      providers: [
        AuthService,
        {
          provide: ActivatedRoute,
          useValue: activatedRouteSpy,
        },
      ],
    }).compileComponents();

    // El fixture nos ayuda a tener acceso al HTML
    fixture = TestBed.createComponent(CallbackComponent);

    authSvc = TestBed.inject(AuthService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // PRUEBAS

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe obtener el Codigo de la URL', () => {
    let code = 'test';
    activatedRouteSpy.queryParams = of({ code: code });
    component.getCodeFromURL();
    expect(component.code).toContain(code);
  });

  it('Debe redirigir al Inicio al NO obtener el codigo', () => {
    activatedRouteSpy.queryParams = of({ code: '' });

    const spy = spyOn(component, 'reDirectTo').and.callFake(() => {});

    component.getCodeFromURL();

    expect(spy).toHaveBeenCalled();
  });

  it('Debe regirigir al Inicio al FALLAR el obtener codigo', () => {
    activatedRouteSpy.queryParams = throwError(() => {});

    const spy = spyOn(component, 'reDirectTo');

    component.getCodeFromURL();

    expect(spy).toHaveBeenCalled();
  });

  it('Debe llamar al servicio para obtener el codigo', () => {
    const tokenFalse: tokenResponse = {
      access_token: 'tokenTest',
      token_type: '',
      expires_in: 0,
      refresh_token: 'refreshTest',
      scope: '',
    };
    const spy = spyOn(authSvc, 'getTokens').and.returnValue(of(tokenFalse));
    component.getTokens();

    expect(spy).toHaveBeenCalled();
  });
});
