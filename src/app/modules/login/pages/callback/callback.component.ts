import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '@shared/services/auth/auth.service';
import { tokenResponse } from '@core/interfaces/auth.interface';

@Component({
  selector: 'app-callback',
  template: ``,
  styles: [],
})
export class CallbackComponent implements OnInit {
  code: string = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getCodeFromURL();
    this.reDirectTo();
  }

  getCodeFromURL(): void {
    this.activatedRoute.queryParams.subscribe({
      next: (data: Params) => {
        this.code = data['code'];
        if (this.code) this.getTokens();
      },
      error: () => {},
    });
  }

  getTokens(): void {
    this.authService.getTokens(this.code).subscribe({
      next: (data: tokenResponse) => {
        this.saveTokens(data);
      },
      error: () => {},
    });
  }

  saveTokens(tokens: tokenResponse): void {
    this.authService.saveTokens(
      tokens.access_token,
      tokens.refresh_token || ''
    );
  }

  reDirectTo(): void {
    this.router.navigate(['/']);
  }
}
