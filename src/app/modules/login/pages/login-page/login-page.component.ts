import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent {
  constructor() {}

  ngOnInit(): void {}

  redirectToSpotify() {
    location.href = `${environment.authorizeURL}?client_id=${
      environment.clientId
    }&response_type=code&redirect_uri=${encodeURIComponent(
      environment.callbackURL
    )}&scope=${environment.authorizeScope}`;
  }
}
