import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { albumItem } from '@core/interfaces/albums.interface';
import { tracksResponse } from '@core/interfaces/tracks.interface';
import { trackItem } from '../../../core/interfaces/tracks.interface';

@Injectable({
  providedIn: 'root',
})
export class AlbumsService {
  constructor(private http: HttpClient) {}

  getTracks(id: string): Observable<trackItem[]> {
    return this.http
      .get<tracksResponse>(`${environment.baseURL}albums/${id}/tracks`)
      .pipe(map((data: tracksResponse) => data.items));
  }

  getAlbum(id: number): Observable<albumItem> {
    return this.http.get<albumItem>(`${environment.baseURL}albums/${id}`);
  }
}
