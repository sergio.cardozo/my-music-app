import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  playerResponse,
  recentlyPlayed,
} from '@core/interfaces/player.interface';

@Injectable({
  providedIn: 'root',
})
export class PlayerService {
  constructor(private http: HttpClient) {}

  getQuery(url: string) {
    return this.http.get(url);
  }

  getRecentlyPlayed(): Observable<recentlyPlayed[]> {
    return this.getQuery(
      `${environment.baseURL}me/player/recently-played?limit=5`
    ).pipe(map((data: playerResponse) => data.items));
  }
}
