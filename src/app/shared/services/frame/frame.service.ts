import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

interface Imedia {
  type: string;
  id: string;
}

@Injectable({
  providedIn: 'root',
})
export class FrameService {
  media = {
    type: '',
    id: '',
  };
  private mediaPlay = new BehaviorSubject<Imedia>(this.media);

  get selectedMediaPlay(): Observable<Imedia> {
    return this.mediaPlay.asObservable();
  }
  setMedia(id: string, type: string): void {
    const media = {
      id: id,
      type: type,
    };
    this.mediaPlay.next(media);
  }
  constructor() {}
}
