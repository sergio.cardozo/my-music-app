import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { playlistItem } from '@core/interfaces/playlists.interface';
import {
  tracksPlaylistItem,
  tracksResponse,
} from '@core/interfaces/tracks.interface';

@Injectable({
  providedIn: 'root',
})
export class PlaylistsService {
  constructor(private http: HttpClient) {}

  getTracks(id: string): Observable<tracksPlaylistItem[]> {
    return this.http
      .get<tracksResponse>(`${environment.baseURL}playlists/${id}/tracks`)
      .pipe(map((data: tracksResponse) => data.items));
  }

  getPlaylist(id: number): Observable<playlistItem> {
    return this.http.get<playlistItem>(`${environment.baseURL}playlists/${id}`);
  }
}
