import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { favoritesResponse } from '@core/interfaces/library.interface';
import {
  tracksPlaylistItem,
  tracksResponse,
} from '@core/interfaces/tracks.interface';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LibraryService {
  favorites: string[] = [];
  private favorites$ = new BehaviorSubject<string[]>(this.favorites);

  get favoritesTracks$(): Observable<string[]> {
    return this.favorites$.asObservable();
  }

  setFavorite(id: string): void {
    this.favorites.push(id);
    this.favorites$.next(this.favorites);
  }
  removeFavorite(id: string) {
    this.favorites = this.favorites.filter((item) => item !== id);
    this.favorites$.next(this.favorites);
  }

  constructor(private http: HttpClient) {}

  addTrackToFavorites(id: string) {
    return this.http.put(`${environment.baseURL}me/tracks?ids=${id}`, '');
  }

  removeFromFavorites(id: string) {
    return this.http.delete(`${environment.baseURL}me/tracks?ids=${id}`);
  }

  getSavedTracks(): Observable<tracksPlaylistItem[]> {
    return this.http
      .get<tracksResponse>(`${environment.baseURL}me/tracks`)
      .pipe(map((data: tracksResponse) => data.items));
  }

  getIdSavedTracks() {
    return this.getSavedTracks().subscribe((data) => {
      this.favorites = data.map((item) => item.track.id);
      this.setFavorite('');
    });
  }
}
