import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { albumItem } from '@core/interfaces/albums.interface';
import { browseResponse, Categories } from '@core/interfaces/browse.interface';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { playlistItem } from '@core/interfaces/playlists.interface';

@Injectable({
  providedIn: 'root',
})
export class BrowseService {
  constructor(private http: HttpClient) {}

  getQuery(url: string) {
    return this.http.get(url);
  }

  getCategories(offset: number): Observable<Categories> {
    return this.getQuery(
      `${environment.baseURL}browse/categories?country=CO&offset=${offset}&limit=3`
    ).pipe(
      map((data: browseResponse) => {
        return data.categories;
      })
    );
  }

  getNewReleases(): Observable<albumItem[]> {
    return this.getQuery(
      `${environment.baseURL}browse/new-releases?limit=10`
    ).pipe(map((data: browseResponse) => data.albums.items));
  }

  getPlaylists(id: string): Observable<playlistItem[]> {
    return this.getQuery(
      `${environment.baseURL}browse/categories/${id}/playlists?limit=10`
    ).pipe(map((data: browseResponse) => data.playlists.items));
  }
}
