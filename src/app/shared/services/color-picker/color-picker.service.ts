import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ColorPickerService {
  constructor() {}

  getAverageRGB(image: HTMLImageElement) {
    const blockSize = 5;
    const rgb = { r: 0, g: 0, b: 0 };
    const canvas = document.createElement('canvas');
    const context: CanvasRenderingContext2D = canvas.getContext('2d');
    let data: ImageData;
    let i = -4;
    let length = 0;
    let count = 0;
    canvas.width = image.naturalWidth;
    canvas.height = image.naturalHeight;

    if (!context) {
      return rgb;
    }
    context.drawImage(image, 0, 0);

    try {
      data = context.getImageData(0, 0, canvas.width, canvas.height);
    } catch (e) {
      return rgb;
    }

    length = data.data.length;

    while ((i += blockSize * 4) < length) {
      ++count;
      rgb.r += data.data[i];
      rgb.g += data.data[i + 1];
      rgb.b += data.data[i + 2];
    }
    rgb.r = ~~(rgb.r / count);
    rgb.g = ~~(rgb.g / count);
    rgb.b = ~~(rgb.b / count);

    return rgb;
  }
}
