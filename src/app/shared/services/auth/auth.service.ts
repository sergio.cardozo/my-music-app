import { Injectable } from '@angular/core';
import { HttpClient, HttpContext, HttpParams } from '@angular/common/http';
import { StorageService } from '../storage/storage.service';
import { map, Observable } from 'rxjs';
import { tokenResponse } from '@core/interfaces/auth.interface';
import { environment } from 'src/environments/environment';
import { AUTHENTICATION_HEADER } from '@core/interceptors/main/main.interceptor';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private router: Router
  ) {}

  getQuery(params: HttpParams): Observable<tokenResponse> {
    return this.http.post<tokenResponse>(environment.getTokenURL, '', {
      params,
      context: new HttpContext().set(AUTHENTICATION_HEADER, true),
    });
  }

  getTokens(code: string): Observable<tokenResponse> {
    const params = new HttpParams()
      .set('grant_type', 'authorization_code')
      .set('code', code)
      .set('redirect_uri', environment.callbackURL);

    return this.getQuery(params);
  }

  refreshToken(): Observable<string> {
    const params: HttpParams = new HttpParams()
      .set('grant_type', 'refresh_token')
      .set('refresh_token', this.storage.getRefreshToken());

    return this.getQuery(params).pipe(
      map((data: tokenResponse) => data.access_token)
    );
  }

  saveTokens(accessToken: string, refreshToken: string) {
    this.storage.saveToken(accessToken);
    this.storage.saveRefreshToken(refreshToken);
  }

  logOut() {
    this.storage.clearStorage();
    this.router.navigate(['/', 'login']);
  }
}
