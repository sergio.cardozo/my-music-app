import { Component, Input, OnInit } from '@angular/core';
import { collection } from '@core/interfaces/collection.interface';

@Component({
  selector: 'app-media-card',
  templateUrl: './media-card.component.html',
  styleUrls: ['./media-card.component.scss'],
})
export class MediaCardComponent implements OnInit {
  @Input() media: collection;

  constructor() {}

  ngOnInit(): void {}
}
