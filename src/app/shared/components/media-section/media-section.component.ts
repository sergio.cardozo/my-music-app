import { Component, Input, OnInit } from '@angular/core';
import { PlayerService } from '@shared/services/player/player.service';
import { BrowseService } from '@shared/services/browse/browse.service';
import { playlistItem } from '@core/interfaces/playlists.interface';
import { albumItem } from '@core/interfaces/albums.interface';

@Component({
  selector: 'app-media-section',
  templateUrl: './media-section.component.html',
  styleUrls: ['./media-section.component.scss'],
})
export class MediaSectionComponent implements OnInit {
  @Input() id: string = '';
  medialist = [];

  collapsed: boolean = true;

  constructor(private browse: BrowseService, private player: PlayerService) {}

  ngOnInit(): void {
    if (this.id === 'new-releases') {
      this.browse.getNewReleases().subscribe((data: albumItem[]) => {
        this.filterCollection(data);
      });
    } else {
      this.browse.getPlaylists(this.id).subscribe((data: playlistItem[]) => {
        this.filterCollection(data);
      });
    }
  }

  filterCollection(data: albumItem[] | playlistItem[]) {
    this.medialist = data;
    this.medialist = this.medialist.filter((data) => data);
  }

  toggleCollapsed() {
    this.collapsed = !this.collapsed;
  }
}
