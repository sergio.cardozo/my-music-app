import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/services/auth/auth.service';

interface link {
  name: string;
  icon: string;
  router: string[];
}

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {
  collapsed: boolean = true;

  linksSidebar: link[] = [
    {
      name: 'Inicio',
      icon: 'fa-solid fa-house',
      router: ['/'],
    },
    {
      name: 'Buscar',
      icon: 'fa-solid fa-magnifying-glass',
      router: ['/', 'search'],
    },
    {
      name: 'Tu Biblioteca',
      icon: 'fa-solid fa-headphones',
      router: ['/', 'library'],
    },
    {
      name: 'Canciones que te gustan',
      icon: 'fa-solid fa-heart',
      router: ['/', 'favorites'],
    },
  ];
  constructor(private auth: AuthService) {}

  ngOnInit(): void {}

  toggleCollapsed() {
    this.collapsed = !this.collapsed;
  }

  logOut() {
    this.auth.logOut();
  }
}
