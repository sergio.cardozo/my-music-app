import { Component, OnInit } from '@angular/core';
import { SpinnerLoadingService } from '../../services/spinner-loading/spinner-loading.service';

@Component({
  selector: 'app-spinner-loading',
  templateUrl: './spinner-loading.component.html',
  styleUrls: ['./spinner-loading.component.scss'],
})
export class SpinnerLoadingComponent implements OnInit {
  constructor(private spinner: SpinnerLoadingService) {}
  isLoading = this.spinner.isLoading;
  ngOnInit(): void {}
}
