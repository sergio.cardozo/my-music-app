import {
  Component,
  ElementRef,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ArtistsService } from '@shared/services/artists/artists.service';
import { artistResponse } from '@core/interfaces/artist.interface';
import { albumItem } from '@core/interfaces/albums.interface';
import { playlistItem } from '@core/interfaces/playlists.interface';
import { ColorPickerService } from '@shared/services/color-picker/color-picker.service';
import { DOCUMENT } from '@angular/common';
import { favoritesCollection } from '@core/interfaces/collection.interface';

@Component({
  selector: 'app-header-collection',
  templateUrl: './header-collection.component.html',
  styleUrls: ['./header-collection.component.scss'],
})
export class HeaderCollectionComponent implements OnInit {
  @Input() type: string;
  @Input() collection: albumItem | playlistItem | favoritesCollection;
  @ViewChild('image') image: ElementRef<HTMLImageElement>;
  artist: artistResponse;

  colorImage = { r: 0, g: 0, b: 0 };
  constructor(
    private artists: ArtistsService,
    private color: ColorPickerService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {
    if (this.type === 'album') {
      this.artists
        .getArtist(this.collection['artists'][0].id)
        .subscribe((dataArtist: artistResponse) => {
          this.artist = dataArtist;
        });
    }
  }

  getColor() {
    this.colorImage = this.color.getAverageRGB(this.image.nativeElement);
    this.applyColor();
  }

  applyColor() {
    let r = this.colorImage.r;
    let g = this.colorImage.g;
    let b = this.colorImage.b;

    // TODO : Cambiar por variables bien hechas SASS
    this.document.documentElement.style.setProperty(
      '--color-1',
      `rgb(${r} ${g} ${b})`
    );
    this.document.documentElement.style.setProperty(
      '--color-7',
      `rgb(${r} ${g} ${b})`
    );
  }
}
