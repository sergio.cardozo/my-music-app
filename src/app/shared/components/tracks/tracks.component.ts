import { Component, Input, OnInit } from '@angular/core';
import { albumItem } from '@core/interfaces/albums.interface';
import { playlistItem } from '@core/interfaces/playlists.interface';
import { AlbumsService } from '@shared/services/albums/albums.service';
import { PlaylistsService } from '@shared/services/playlists/playlists.service';
import { trackItem } from '@core/interfaces/tracks.interface';
import { LibraryService } from '@shared/services/library/library.service';
import { FrameService } from '@shared/services/frame/frame.service';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.scss'],
})
export class TracksComponent implements OnInit {
  @Input() type: string;
  @Input() collection: albumItem | playlistItem;
  favorites = this.library.favoritesTracks$;
  tracks = [];

  getTracks = {
    album: () => {
      this.albums
        .getTracks(this.collection.id)
        .subscribe((data: trackItem[]) => {
          this.tracks = data;
        });
    },
    playlist: () => {
      this.playlists.getTracks(this.collection.id).subscribe((data) => {
        this.tracks = data.map((track) => track.track);
      });
    },
    favorites: () => {
      this.library.getSavedTracks().subscribe((data) => {
        this.tracks = data.map((track) => track.track);
      });
    },
  };

  constructor(
    private albums: AlbumsService,
    private playlists: PlaylistsService,
    private library: LibraryService,
    private frame: FrameService
  ) {}

  ngOnInit(): void {
    this.getTracks[this.type]();
    this.getIdSavedTracks();
  }

  playMedia() {
    this.frame.setMedia(this.collection.id, this.type);
  }

  playTrack(id: string) {
    this.frame.setMedia(id, 'track');
  }

  toggleFavorite(id: string) {}

  addFavorite(id: string) {
    this.library.addTrackToFavorites(id).subscribe();
    this.library.setFavorite(id);
  }
  removeFavorite(id: string) {
    this.library.removeFromFavorites(id).subscribe();
    this.library.removeFavorite(id);
  }

  getIdSavedTracks() {
    this.library.getIdSavedTracks();
  }
}
