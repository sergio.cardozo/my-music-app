import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { MediaPlayerComponent } from './components/media-player/media-player.component';
import { RouterModule } from '@angular/router';
import { MediaCardComponent } from './components/media-card/media-card.component';
import { MediaSectionComponent } from './components/media-section/media-section.component';
import { SpinnerLoadingComponent } from './components/spinner-loading/spinner-loading.component';
import { HeaderCollectionComponent } from './components/header-collection/header-collection.component';
import { TracksComponent } from './components/tracks/tracks.component';
import { SafePipe } from './pipes/safe/safe.pipe';
import { DescriptionPipe } from './pipes/description/description.pipe';

const components = [
  SideBarComponent,
  MediaPlayerComponent,
  MediaCardComponent,
  MediaSectionComponent,
  SpinnerLoadingComponent,
  HeaderCollectionComponent,
  TracksComponent,
  DescriptionPipe,
  SafePipe,
];

@NgModule({
  declarations: [...components],
  imports: [CommonModule, RouterModule],
  exports: [...components],
})
export class SharedModule {}
