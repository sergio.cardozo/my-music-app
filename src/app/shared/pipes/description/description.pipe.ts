import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'description',
})
export class DescriptionPipe implements PipeTransform {
  transform(playlist: any): string {
    if (playlist.description) return playlist.description;
    else if (playlist.artists) return playlist.artists[0].name;
    return null;
  }
}
