import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe',
})

// Este pipe lo uso para el iframe de spotify
export class SafePipe implements PipeTransform {
  private sanitizer;
  constructor() {
    this.sanitizer = DomSanitizer;
  }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
