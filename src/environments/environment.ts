// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  clientId: '742e821cf94b4298a325d10d11c9bb6b',
  clientSecret: '6245befccb98442fa17ad32e5af89046',
  authorizeURL: 'https://accounts.spotify.com/es-ES/authorize',
  authorizeScope:
    'user-read-recently-played+user-library-modify+user-library-read',
  callbackURL: 'http://localhost:4200/login/callback',
  getTokenURL: 'https://accounts.spotify.com/api/token',
  baseURL: 'https://api.spotify.com/v1/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
